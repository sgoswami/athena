#include "TileTBRec/TileTBAANtuple.h"
#include "TileTBRec/TileTBStat.h"
#include "TileTBRec/TileTBDump.h"
#include "TileTBRec/TileDigitsGainFilter.h"
#include "../TileRawChannelGainFilter.h"


DECLARE_COMPONENT( TileTBAANtuple )
DECLARE_COMPONENT( TileTBStat )
DECLARE_COMPONENT( TileTBDump )
DECLARE_COMPONENT( TileDigitsGainFilter )
DECLARE_COMPONENT( TileRawChannelGainFilter )
