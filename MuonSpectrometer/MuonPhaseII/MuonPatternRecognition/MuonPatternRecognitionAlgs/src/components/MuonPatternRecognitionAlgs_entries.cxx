/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../EtaHoughTransformAlg.h"
#include "../PhiHoughTransformAlg.h"
#include "../MuonSegmentFittingAlg.h"
DECLARE_COMPONENT(MuonR4::EtaHoughTransformAlg)
DECLARE_COMPONENT(MuonR4::PhiHoughTransformAlg)
DECLARE_COMPONENT(MuonR4::MuonSegmentFittingAlg)
